import { Component, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';

@Component({
  selector: 'app-soloq',
  templateUrl: './soloq.component.html',
  styleUrls: ['./soloq.component.scss']
})
export class SoloqComponent implements OnInit {

  public tournamentList: any[]
  

  constructor(
    public db: AngularFirestore,
    public storage: AngularFireStorage,
    public router: Router
  ) {
    this.tournamentList = []
   }

  ngOnInit(): void {
     this.getTournaments()
    //this.getTournamentsFilter()

  }
 //cada queue tiene un campo status con el valor {started,finished,starting}
  getTournaments(){
    this.db.collection('queues').get().forEach(tournament => {
      var data;
      tournament.forEach(e => {
        data = e.data()
        if(this.queueStatusHelper(data) != data.status){
          this.db.collection('queues').doc(e.id).set({
            status:this.queueStatusHelper(data)
          },{merge:true})
        } 
        data["status"]=this.queueStatusHelper(data)
        this.tournamentList.push(data)  
        console.log("data",data)
      })
      
    })
  }

  getTournamentsFilter(){
    this.db.collection('queues').ref.where("status","==","starting").get().then(docs => {
      docs.forEach(doc => {
        console.log(doc.data())
      })
    })
  }
  queueDetail(queue){
    console.log(queue)
    this.router.navigate(['queue-page'],{state:{queueDetail:queue}})
  }
  getParticipantsLength(arrParticipants:[]){
    return arrParticipants.length
  }

  queueStatusHelper(queue){
    var dNow = new  Date().getTime()
    var dateQueueEnd = new Date(queue.dateEnd+' 00:00:00')
    console.log("dayInput",dateQueueEnd.getTime()) 
    var dateQueueStart = new Date(queue.dateStart+' 00:00:00') 
    console.log(dateQueueStart.getTime())

    if(dNow< dateQueueEnd.getTime()){
      if(dNow< dateQueueStart.getTime()){
        return "starting"
      }
      if(dNow> dateQueueStart.getTime()){
        return "started"
      }
    }else{
      console.log("hola")
      return "finished"
    }
  }

}
