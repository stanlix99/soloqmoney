import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SoloqComponent } from './soloq.component';

describe('SoloqComponent', () => {
  let component: SoloqComponent;
  let fixture: ComponentFixture<SoloqComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SoloqComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SoloqComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
