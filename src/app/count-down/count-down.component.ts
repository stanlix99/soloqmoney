import { Input, OnDestroy } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Subscription, interval } from 'rxjs';

@Component({
  selector: 'app-count-down',
  templateUrl: './count-down.component.html',
  styleUrls: ['./count-down.component.scss']
})
export class CountDownComponent implements OnInit,OnDestroy {
  @Input() queue : any;
  private subscription: Subscription;
  
  public dateNow = new Date().toLocaleString;
  public dDay;
  public dDayEnd;
  milliSecondsInASecond = 1000;
  hoursInADay = 24;
  minutesInAnHour = 60;
  SecondsInAMinute  = 60;

  public timeDifference;
  public secondsToDday;
  public minutesToDday;
  public hoursToDday;
  public daysToDday;



  private getTimeDifference () {
    if(this.queue.status == "starting"){
      this.timeDifference = this.dDay.getTime() - new  Date().getTime();
      this.allocateTimeUnits(this.timeDifference);
      
    }else if(this.queue.status == "started"){
      this.timeDifference = this.dDayEnd.getTime() - new Date().getTime();
      this.allocateTimeUnits(this.timeDifference);
    }
  }

private allocateTimeUnits (timeDifference) {
      this.secondsToDday = Math.floor((timeDifference) / (this.milliSecondsInASecond) % this.SecondsInAMinute);
      this.minutesToDday = Math.floor((timeDifference) / (this.milliSecondsInASecond * this.minutesInAnHour) % this.SecondsInAMinute);
      this.hoursToDday = Math.floor((timeDifference) / (this.milliSecondsInASecond * this.minutesInAnHour * this.SecondsInAMinute) % this.hoursInADay);
      this.daysToDday = Math.floor((timeDifference) / (this.milliSecondsInASecond * this.minutesInAnHour * this.SecondsInAMinute * this.hoursInADay));
}

  ngOnInit() {
    this.dDay = new Date(this.queue.dateStart+' 00:00:00');
    this.dDayEnd = new Date(this.queue.dateEnd+' 00:00:00');
     this.subscription = interval(1000)
         .subscribe(x => { this.getTimeDifference(); });
  }

 ngOnDestroy() {
    this.subscription.unsubscribe();
 }
 queueStatusHelper(){
  var dNow = new  Date().getTime()
  var dateQueueEnd = new Date(this.queue.dateEnd+' 00:00:00')
  console.log("dayInput",dateQueueEnd.getTime()) 
  var dateQueueStart = new Date(this.queue.dateStart+' 00:00:00') 
  console.log(dateQueueStart.getTime())

  if(dNow < dateQueueEnd.getTime()){
    if(dNow < dateQueueStart.getTime()){
      return "starting"
    }
    if(dNow > dateQueueStart.getTime()){
      return "started"
    }
  }else{
    console.log("hola")
    return "finished"
  }
}

}
