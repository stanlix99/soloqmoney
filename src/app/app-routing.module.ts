import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AuthGuardService } from './auth-guard.service';
import { ProfileComponent } from './profile/profile.component';
import { SoloqComponent } from './soloq/soloq.component';
import { QueuePageComponent } from './queue-page/queue-page.component';

const routes: Routes = [
   {path: '', redirectTo: 'login', pathMatch: 'full'},
  {path: 'home', component: HomeComponent,canActivate:[AuthGuardService]},
  {path: 'register', component: RegisterComponent},
  {path: 'login', component: LoginComponent},
  {path: 'profile', component: ProfileComponent},
  {path: 'soloq', component: SoloqComponent},
  {path: 'queue-page', component: QueuePageComponent},






];

@NgModule({
  imports: [RouterModule.forRoot(routes,)],
  exports: [RouterModule]
})
export class AppRoutingModule {
  
 }
