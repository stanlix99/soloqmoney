import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../services/interfaces'

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  private API_KEY = 'RGAPI-0cf69377-869d-4877-8984-7d698663f772'
  
 
  constructor(private http: HttpClient) {
  }

  getUserUUID(region,summonerName):Observable<any>{
    let summonerNameURI = encodeURIComponent(summonerName)
    const httpOptions = {
      headers: new HttpHeaders({
        "X-Riot-Token": this.API_KEY,
        "Content-Type":" application/json",
        "Accept-Language": "es-ES,es;q=0.9",

      })
    };


    return this.http.get<any>("https://euw1.api.riotgames.com/lol/summoner/v4/summoners/by-name/"+summonerName,httpOptions);
  }

  getUserRankedInfo(region,encryptedId){

    const httpOptions = {
      headers: new HttpHeaders({
        "X-Riot-Token": "RGAPI-f9b59508-1031-46b7-87e9-b52513cdf53f",
        "Content-Type":" application/json",
        "Accept-Language": "es-ES,es;q=0.9",

        
      })
    };



    return this.http.get<any>("https://euw1.api.riotgames.com/lol/league/v4/entries/by-summoner/"+encryptedId,httpOptions)
  }
 
  getFakeApi(){
    return this.http.get<any>('https://jsonplaceholder.typicode.com/todos/1')
  }
  
}


