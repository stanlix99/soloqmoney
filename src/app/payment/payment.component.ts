import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
declare var paypal;
@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {
  public error:boolean = false
  @Input() queue : any;
  @Input() user : any;

  @ViewChild('paypal',{static: true}) paypalElement: ElementRef

  public paidFor=false;
  constructor(private db :AngularFirestore) { }

  ngOnInit(): void {
    paypal.Buttons({createOrder:(data,actions) => {
      return actions.order.create({
        purchase_units: [
          {
            description: this.queue.description,
            amount:{ 
              currency_code:'EUR',
              value: this.queue.entryFee
            }
          }
        ]
      })
    },
    onApprove: async (data,actions) =>{
      const order = await actions.order.capture();
      this.paidFor = true;
      
      //add entry al array de participantes de queue de firestore
      this.db.collection('queues').doc(this.queue.queueId).ref.get().then(doc => {
       var queueRes : any =  doc.data()
       queueRes.signedUp.push(this.user.uid)
       this.db.collection('queues').doc(this.queue.queueId).ref.set({
         signedUp:queueRes.signedUp
       },{merge:true})
      }).then(onComplete => {
        this.user.queuePurchased.push(this.queue.queueId)
        this.db.collection('users').doc(this.user.uid).ref.set({queuePurchased:this.user.queuePurchased},{merge:true})
        setTimeout(()=>{                          
          window.location.reload()
     }, 3000);
        
      })
      
 
    },
    onError: error =>{
      console.log(error)
      this.error = true;
    }
  })
  .render(this.paypalElement.nativeElement)
  }
  delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
}

}
