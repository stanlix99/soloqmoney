import { Component, Input, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { HttpClient } from '@angular/common/http';
import { HttpService } from '../../app/services/http.service'
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-leaderboard',
  templateUrl: './leaderboard.component.html',
  styleUrls: ['./leaderboard.component.scss']
})
export class LeaderboardComponent implements OnInit {
  @Input() queue : any;
  public participants: any[];
  public data:any;
  public rankedUserInfo:any[];
  public showLeaderboard: boolean
  private tierMap: any;
  private rankMap: any;
  public sortedList:any[];
  

  constructor(public db: AngularFirestore,private http: HttpClient,private apiRiot:HttpService) {
    this.showLeaderboard = false;
    this.tierMap = {
      CHALLENGER:90000,
      GRANDMASTER:80000,
      MASTER : 70000,
      DIAMOND : 60000,
      PLATINUM: 50000,
      GOLD    : 40000,
      SILVER  : 30000,
      BRONZE : 20000,
      IRON : 10000,
    }
    this.rankMap = {
      I:400,
      II:300,
      III:200,
      IV:100
    }
   }

  async ngOnInit(): Promise<void> {

  this.getQueueSignedUp()
  this.delay(500).then(() => {
    this.getRankedInfo()
  })
  this.delay(1100).then(() => {
    this.participants.sort((obj1, obj2) => {
      console.log("OBJ1",obj1.leaderPoints,"OBJ2",obj2.leaderPoints)
      if (obj1.leaderPoints < obj2.leaderPoints) {
          return 1;
      }
  
      if (obj1.leaderPoints > obj2.leaderPoints) {
          return -1;
      }
  
      return 0;
  });
  })
  }
// METER INFO RANKED DENTRO DEL OBJETO DATA POR CADA PARTICIPANTE
    getQueueSignedUp(){
    this.participants = []
    this.queue.signedUp.forEach( element => {
    var userRef = this.db.collection("users").doc(element).ref
      userRef.get().then( doc => {
        this.data = doc.data()
        console.log("users",this.data)
        this.participants.push(this.data)
    })
  })
  
  }
   delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
}

   getRankedInfo(){
      console.log("listRn",this.data)
      console.log("participants",this.participants)
      try{
        this.participants.forEach(  element => {
          console.log("server",this.queue.server,"encry",element.linkedAccount.encryptedId)
            this.apiRiot.getUserRankedInfo(this.queue.server,element.linkedAccount.encryptedId).subscribe( async rankedInfo => {
              await rankedInfo.forEach( queueEntry => {
                if(queueEntry.queueType == "RANKED_SOLO_5x5"){
                  element["rankedInfo"]=queueEntry
                  element["leaderPoints"] = this.getLeaderPoints(queueEntry.tier,queueEntry.rank,queueEntry.leaguePoints)
                  console.log(queueEntry.summonerName)
               }
              })

             
          },error => console.log('oops', error))
        })
      }catch(error){
        console.log("damn")
      }
      
  }
  //metodo para ordenar la lista, basada en el tier, rank y LP
  getLeaderPoints(tier,rank,lp){
    var leaderPoints = this.tierMap[tier] + this.rankMap[rank] + lp
    return leaderPoints
  }
  getOpgg(summonerName){
    return "https://euw.op.gg/summoner/userName="+summonerName
  }
  
}
