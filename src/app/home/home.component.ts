import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../app/services/http.service'


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private HttpService:HttpService) { }

  ngOnInit(): void {
    this.HttpService.getFakeApi().subscribe(data => {
      console.log("fake api",data)
    })
    

  }
}
