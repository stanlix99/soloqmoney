import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.scss']
})
export class FriendsComponent implements OnInit {
  public userData :any;
  public match: false;
  public peopleList: any[]
  public data:any;
  public listSearch : any[]
  public dataSearch : any
  public sortedSearchList : any[]
  private personSelected : any;

  public searchForm = new FormGroup({
    searchText: new FormControl('',[Validators.required,Validators.minLength(4),Validators.maxLength(16),Validators.pattern('^[a-zA-Z0-9_]+( [a-zA-Z0-9_]+)*$')]),
  });


  constructor(
    public db : AngularFirestore, 
  ) {
    this.peopleList = [''];
   }

  ngOnInit(): void {
    this.userData = JSON.parse(sessionStorage.getItem('currentUser'))
    console.log("account",this.userData)

    this.checkPeopleDb()
    
  }

  searchFunction(){
    this.listSearch = []
   var personNickname = this.searchForm.value.searchText
   var peopleRef = this.db.collection('people').ref
   peopleRef.get().then((listSorted) => {
     listSorted.forEach(nickname => {
       console.log('matches',nickname.data())
       this.dataSearch = nickname.data()
       console.log('datasaearch',this.dataSearch)
       this.listSearch = this.dataSearch.nicknames
       var searchQuery = this.searchForm.value.searchText
       this.sortedSearchList = this.listSearch.filter(function(nick){
         return String(nick).startsWith(searchQuery)
       })
       console.log('lista',this.sortedSearchList)
     })
   })

  }

  checkPeopleDb(){
    this.db.collection('people').doc('peopleList').ref.get().then(doc => {
     this.data = doc.data()
      this.peopleList = this.data.nicknames   
    })
    var peopleRef = this.db.collection('people').ref
    
    peopleRef.get().then(docs => {
      peopleRef.where("nicknames","array-contains",this.userData.nickname).get()
      .then((querySnapshot) => {
       if(querySnapshot.size == 0){
         this.peopleList.push(this.userData.nickname)
         this.db.collection('people').doc("peopleList").set({
          nicknames: this.peopleList
        })
       }   
      })
    })
  }
  sendFriendReq(nickname){
    this.db.collection('users').ref.where("nickname",'==',nickname).get().then(docs => {
      docs.forEach(doc => {
        //Solo devuelve 1 entrada ya que el nickname es unico.
        this.personSelected = doc.data()
        // creo el obj que ira en el document
        var friendRequest = {
          uidRequester : this.userData.uid,
          accepted : false
        }
        this.db.collection('users').doc(this.personSelected.uid).collection('friendReq').add({
          friendRequest: friendRequest
        })
      })
    })

  }

}
