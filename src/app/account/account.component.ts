import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AngularFireModule } from '@angular/fire';
import * as firebase from 'firebase';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { HttpService } from '../services/http.service'
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import { MatSelect } from '@angular/material/select'
import { User } from '../services/interfaces';
import { AngularFireStorage } from '@angular/fire/storage';


@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {
  public userData :any;
  public dialogRef:MatDialogRef<DialogLinkAccount>;
  fileToUpload: File | null = null;
  
  public nicknameForm = new FormGroup({
    nickname: new FormControl('',[Validators.required,Validators.minLength(4),Validators.maxLength(16),Validators.pattern('^[a-zA-Z0-9_]+( [a-zA-Z0-9_]+)*$')]),
  });
  public assistService = new FormGroup({
    textTicket: new FormControl('',[Validators.required,Validators.minLength(20),Validators.maxLength(200)]),
  });
  avatarUrl: any;
  peopleList: any[];
  data: any;


  constructor(
    public db : AngularFirestore,
    public router:Router,
    public dialog: MatDialog,
    public storage :AngularFireStorage
    )
    {

   }

  ngOnInit(): void {
  
    this.userData = JSON.parse(sessionStorage.getItem('currentUser'))
    console.log("account",this.userData)
    

  }
  openLinkedAccountDialog(){
    this.dialogRef = this.dialog.open(DialogLinkAccount, {
      width:'80vw',
      height:'65vw',
      
    })
  }


  changeNickname(){

    if(this.nicknameForm.valid){
      this.db.collection('users').doc(this.userData.uid).ref.set({
        nickname:this.nicknameForm.value.nickname
      },{merge:true}).catch(error => {
        alert("Error, your nickname couldn't be updated");
      }).then(onSuccess => {
      //Volvemos a cargar los datos en el sessionStorage
      this.db.collection("users").doc(this.userData.uid).ref.get().then(doc => {
        console.log("USUARIO FRSTORE",doc.data())
        var user = doc.data()
        sessionStorage.removeItem("currentUser")
        sessionStorage.setItem("currentUser",JSON.stringify(user))
        console.log("updated user", JSON.parse(sessionStorage.getItem("currentUser")))
        // actualizar el usuario de la lista people
        
        this.db.collection('people').doc('peopleList').ref.get().then(doc => {
          this.data = doc.data()
           this.peopleList = this.data.nicknames   
           console.log(this.peopleList)
         })
         var peopleRef = this.db.collection('people').ref
         
         peopleRef.get().then(docs => {
           console.log(this.userData.nickname)
           peopleRef.where("nicknames","array-contains",this.userData.nickname).get()
           .then((querySnapshot) => {
             console.log(querySnapshot.size)
            if(querySnapshot.size != 0){
              var index = this.peopleList.indexOf(this.userData.nickname)
              console.log(index)

              this.peopleList.splice(index,1)
              this.peopleList.push(this.nicknameForm.value.nickname)
              console.log(this.peopleList)
              this.db.collection('people').doc("peopleList").set({
               nicknames: this.peopleList
             })
            }   
           }).then(function(){
             window.location.reload()
           })
         })

      })
      })
    }
  }
  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
    console.log(this.fileToUpload)
  }
  uploadProfileIcon(){
    if(this.fileToUpload.name.endsWith(".jpg") || this.fileToUpload.name.endsWith(".png")||this.fileToUpload.name.endsWith(".jpeg")){
      if(this.fileToUpload.size < 2000000){
        var avatarRef = this.storage.ref("/avatars/"+this.makeid(7))
        avatarRef.put(this.fileToUpload).then((snapshot) => {
          snapshot.ref.getDownloadURL().then(url => {
            this.db.collection('users').doc(this.userData.uid).ref.set({avatar:url},{merge:true}).then(onSuccess => {
              //Volvemos a cargar los datos en el sessionStorage
              this.db.collection("users").doc(this.userData.uid).ref.get().then(doc => {
                console.log("USUARIO FRSTORE",doc.data())
                var user = doc.data()
                sessionStorage.removeItem("currentUser")
                sessionStorage.setItem("currentUser",JSON.stringify(user))
                console.log("updated user", JSON.parse(sessionStorage.getItem("currentUser")))
              }).then(reload => {
                window.location.reload()
              })
              })
          })
          
        })
        
        
      }
    }
  }
   makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * 
 charactersLength));
   }
   return result;
}

  logOut(){
    console.log('logout')
    sessionStorage.clear()
    console.log(JSON.parse(sessionStorage.getItem("currentUser")))
    this.router.navigate(['login'])
  }
  sendTicket(){
    if(this.assistService.valid){
      let date: Date = new Date();
      this.db.collection('tickets').add({
        userDocId:this.userData.uid,
        email:this.userData.email,
        message:this.assistService.value.textTicket,
        date:date.toUTCString()
      
      }).then(() => {
        this.assistService.setValue({textTicket : ''})
      })
    }
  }


  
}

@Component({
  selector: 'dialog-link-account',
  templateUrl: 'dialog-link-account.html',
  styleUrls: ['dialog-link-account.scss']
})
export class DialogLinkAccount {
  public region = "No server selected";
  public tagline = "#";
  public summonerExist:boolean;
  public accInfo:any;
  public iconId:any;
  public verified: boolean;
  public db : AngularFirestore;
  public userData :any;


  
  constructor(private apiRiot:HttpService,private dbF:AngularFirestore){
    this.summonerExist = null;
    this.verified = false
    this.userData = JSON.parse(sessionStorage.getItem('currentUser'))
    console.log("datauser",this.userData)
  }
  
  public linkAccountForm = new FormGroup({
    summonerName: new FormControl('',[Validators.required,Validators.minLength(4),Validators.maxLength(16)]),
    tag: new FormControl('#',[Validators.required,Validators.pattern('^[#][a-zA-Z0-9]*')])
  });

  linkAccount(){
    if(this.linkAccountForm.valid){
        this.apiRiot.getUserUUID(this.region,this.linkAccountForm.value.summonerName).subscribe(data =>{
          //CONTROLAR SUMMONER EXISTE
          this.summonerExist = true;
          console.log("UUID",data)
          this.accInfo = data;
          this.randomIcon()

        },(error:any) =>{
          //CONTROLAR SUMMONER NO EXISTE
          this.summonerExist = false;
        })
    }
  }

  randomIcon(){
    let idSame=true;
    while(idSame == true){
      let randomIconId = Math.floor(Math.random()*29)
      if(this.accInfo.profileIconId != randomIconId){
        this.iconId = randomIconId
        console.log(this.iconId)
        idSame = false
      }
    }
  }
  checkIconChanged(){

    this.apiRiot.getUserUUID(this.region,this.linkAccountForm.value.summonerName).subscribe(data =>{
      //CONTROLAR SUMMONER EXISTE
      if(data.profileIconId == this.iconId ){
        this.verified = true
        this.updateUserAccLinked()
      }

    },(error:any) =>{
      //CONTROLAR SUMMONER NO EXISTE
      console.log(error)
      this.verified = false;
    })
  }
  updateUserAccLinked(){
    console.log("uid",this.userData.uid)
    this.dbF.collection('users').doc(this.userData.uid).ref.set({
      linkedAccount: {
        summonerName: this.linkAccountForm.value.summonerName,
        region: this.region,
        encryptedId:this.accInfo.id
        
      }
    },{merge:true}).then(then => {
      //Volvemos a cargar los datos en el sessionStorage
      this.dbF.collection("users").doc(this.userData.uid).ref.get().then(doc => {
        console.log("USUARIO FRSTORE",doc.data())
        var user = doc.data()
        sessionStorage.removeItem("currentUser")
        sessionStorage.setItem("currentUser",JSON.stringify(user))
        console.log("updated user", JSON.parse(sessionStorage.getItem("currentUser")))
      }).then(onComplete => {
        //Recarga la pagina
        window.location.reload();

      })
    })
  }
}
    

