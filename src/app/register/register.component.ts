import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  public register = new FormGroup({
    nickname: new FormControl('', [ Validators.minLength(4),Validators.required,Validators.maxLength(16),Validators.pattern('^[a-zA-Z0-9_]+( [a-zA-Z0-9_]+)*$')]),
    email: new FormControl('', [Validators.required,Validators.pattern('[A-Za-z0-9._%-]+@[A-Za-z0-9._%-]+\\.[a-z]{2,3}')]),
    password: new FormControl('', [Validators.required , Validators.pattern('^(?=[^A-Z]*[A-Z])(?=[^a-z]*[a-z])(?=\\D*\\d)[A-Za-z\\d!$%@#£€*?&]{8,}$')

  ]),
    repassword: new FormControl(''),

  });
  
  public newUser(form){
      console.log(this.register.value)

    if(this.registerFormChecker(form)){
      this.registerAuth(form.email,form.password)
    }



  }


  constructor(public afAuth:AngularFireAuth, public db: AngularFirestore, private router:Router) {
    this.register.setValue({

      nickname: '',
      password: '',
      repassword: '',
      email: '',
    })
   }

  ngOnInit(): void {

  }
  scrollDown(){
    window.scrollTo(0,document.body.scrollHeight);
  }

  registerAuth(user,pass){
    var signedUp; 
    
    this.afAuth.createUserWithEmailAndPassword(user,pass).then(res => {
      res.user.updateProfile({
        displayName: this.register.value.nickname,
      })
      this.db.collection('users').doc(res.user.uid).set({
        nickname:this.register.value.nickname,
        uid:res.user.uid,
        friends:[],
        phone:res.user.phoneNumber,
        avatar:null,
        email:res.user.email,
        linkedAccount:null,
        premium:"false",
        banned:"false",
        userType:"guest"
      }).then(doc =>{
       this.db.collection('users').doc(res.user.uid).set({
         doc_id: res.user.uid
       },{merge:true})
      })
      
    }).catch(e => {
      switch (e.code) {
        case 'auth/email-already-in-use':
          console.log(`Email address ${user} already in use.`);
          signedUp = `Email address ${user} already in use.`
          break;
        case 'auth/invalid-email':
          console.log(`Email address ${user} is invalid.`);
          signedUp = `Email address ${user} is invalid.`
          break;
        case 'auth/operation-not-allowed':
          console.log(`Error during sign up.`);
          signedUp = `Error during sign up.`
          break;
        case 'auth/weak-password':
          console.log('Password is not strong enough. Add additional characters including special characters and numbers.');
          signedUp = 'Password is not strong enough. Add additional characters including special characters and numbers.'
          break;
        default:
          console.log(e.message);
          break;
      }
    }).then(checkIfRegister => {
      
      console.log('signed up',signedUp)
      if(signedUp == null){
        this.router.navigateByUrl('login')
      }else{
        window.alert(signedUp)
      }
    })
    
    
  }

  registerFormChecker(form){
    console.log(this.register)
    if(this.register.controls.nickname.errors){
      window.alert('The nickname must be between 4 and 16 characters')
      return false;
      
    }
    else if(this.register.controls.email.hasError('pattern') || this.register.controls.email.hasError('required') ){
      window.alert('Please enter a valid email address.')
      return false;
    }

     else if(this.register.controls.password.hasError('pattern') || this.register.controls.password.hasError('required') ){
      window.alert('The password must have: 1 uppercase letter 1 lowercase letter A number A minimum length of 8.')
      return false;

     }
    else if(this.register.controls.repassword.value != this.register.controls.password.value){
      window.alert('Passwords do not match.')
      return false;
    }


     return true;

  }
  

}

