import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';


@Component({
  selector: 'app-queue-page',
  templateUrl: './queue-page.component.html',
  styleUrls: ['./queue-page.component.scss'],
})
export class QueuePageComponent implements OnInit {
  public queue: any;
  public currentUser: any;
  public purchased :boolean;
  public dialogRef:MatDialogRef<PaymentDialog>;


    
    constructor(public router:Router,public dialog: MatDialog,private db:AngularFirestore) {
    this.purchased = false;

   }

  ngOnInit(): void {

    this.currentUser = JSON.parse(sessionStorage.getItem("currentUser"))
    this.queue = window.history.state.queueDetail
  }
  openLinkedAccountDialog(){
    this.dialogRef = this.dialog.open(PaymentDialog, {
      width:'80vw',
      height:'65vw',
      data:this.queue
      
    })
  }
  buttonText(){
    if(this.purchased == true){
      return "YOU HAVE ALREADY PURCHASED THIS TICKET"
    }
    if(this.queue.status == 'started' ){
      return "SORRY, THE SOLOQ HAS ALREADY STARTED"
    }else if(this.queue.status =='starting'){
      return "GET YOUR TICKET NOW"
      
    }else if(this.queue.status =='finished'){
      return "SORRY, THE SOLOQ HAS ALREADY ENDED"
      
    }
    
  }
  buttonDisable(){
    var signedUpList :any[] = this.queue.signedUp
    if(signedUpList.indexOf(this.currentUser.uid) != -1){
      this.purchased = true;
      return true
    }else{
      if(this.queue.status != 'starting'){
        return true
      }else{
        return false
      }
    }
  }

}
import {Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'payment-dialog',
  templateUrl: 'payment-dialog.html',
  styleUrls: ['payment-dialog.scss']
})
export class PaymentDialog implements OnInit{
  public currentUser:any;
  constructor(@Inject(MAT_DIALOG_DATA) public queue: any){
  }
  ngOnInit(){
    
    this.currentUser = JSON.parse(sessionStorage.getItem("currentUser"))
  }
}