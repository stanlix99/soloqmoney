import { Component, OnInit } from '@angular/core';
import { AngularFireModule } from '@angular/fire';
import * as firebase from 'firebase';
import { AngularFireAuth } from '@angular/fire/auth';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public login = new FormGroup({
    email: new FormControl('',Validators.required),
    password: new FormControl('', Validators.required),
  });

  constructor(public db :AngularFirestore, private afAuth:AngularFireAuth,private router:Router) {
    if(this.isLoggedIn){
      this.router.navigate(['home'])
    }

   }

  ngOnInit(): void {
  }

  loginMethod() {
    var hasError
    this.afAuth.signInWithEmailAndPassword(this.login.value.email, this.login.value.password)
  .then((userCredential) => {
    this.afAuth.onAuthStateChanged(auth => {
      console.log("ID DE LOG USER",auth.uid)
      this.db.collection("users").doc(auth.uid).ref.get().then(doc => {
        console.log("USUARIO FRSTORE",doc.data())
        var user = doc.data()
        sessionStorage.setItem("currentUser",JSON.stringify(user))

      })
      this.router.navigate(['home'])
    })
  })
  .catch((error) => {
   alert('The email or password are incorrect')
  })
  }
  get isLoggedIn():boolean {

    if(sessionStorage.getItem('currentUser') != null){
      return true;
    }else{
      return false;
    }
  }
}
