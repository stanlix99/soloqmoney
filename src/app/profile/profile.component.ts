import { Component, OnInit } from '@angular/core';
import {MatSidenavModule} from '@angular/material/sidenav'; 

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  showFiller = false;
  public selectSection: String
  constructor() {
    this.selectSection = "account"
   }

  ngOnInit(): void {
    
  }
  selectSectionTab(section){
    this.selectSection = section
    console.log(this.selectSection)
  }

}


