// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.


export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAMAyU-0-47GUsL7h0M_swlBpLw6kea9pM",
    authDomain: "soloqmoney.firebaseapp.com",
    projectId: "soloqmoney",
    storageBucket: "soloqmoney.appspot.com",
    messagingSenderId: "783289753177",
    appId: "1:783289753177:web:30d678d271fbee77059b48",
    measurementId: "G-2YN16T7DVT"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
